CREATE TABLE YV_USERS(
  id INTEGER AUTO_INCREMENT,
  name VARCHAR(100),
  pwd VARCHAR(100),
  privilge VARCHAR(100) NOT NULL,
  age INTEGER,
  mail VARCHAR(100),
  CONSTRAINT usersIdPk PRIMARY KEY(id),
  CONSTRAINT usersPrivilegeFk FOREIGN KEY(privilege) references YV_ACCESS(privilege)
);
CREATE TABLE YV_EVENTS(
  id INTEGER AUTO_INCREMENT,
  theme VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  day DATE NOT NULL,
  description VARCHAR(100) NOT NULL,
  maxEffective INTEGER,
  minEffective INTEGER,
  effective INTEGER,
  address VARCHAR(100) NOT NULL,
  latitude NUMERIC(8, 8),
  longitude NUMERIC(8, 8),
  pending BOOL NOT NULL,
  CONSTRAINT eventsIdPk PRIMARY KEY(id),
  CONSTRAINT eventsThemeFk FOREIGN KEY(theme) references THEMES(name)
);
CREATE TABLE YV_USERSEVENTS(
  uId INTEGER,
  eId INTEGER,
  pending BOOL,
  CONSTRAINT usersEventsUidFk FOREIGN KEY(uId) references YV_USERS(id) ON DELETE CASCADE,
  CONSTRAINT usersEventsEidFk FOREIGN KEY(eId) references YV_EVENTS(id) ON DELETE CASCADE
);
CREATE TABLE YV_ACCESS(
  privilege VARCHAR(100) NOT NULL,
  createDeleteUser BOOL NOT NULL,
  createDeleteTheme BOOL NOT NULL,
  deleteOthersEvents BOOL NOT NULL,
  createDeleteEvent BOOL NOT NULL,
  subUnsubEvent BOOL NOT NULL,
  rateEvent BOOL NOT NULL,
  commentEvent BOOL NOT NULL,
  useSearch BOOL NOT NULL,
  CONSTRAINT accessPrivilegePk PRIMARY KEY(privilege)
);
CREATE TABLE YV_THEMES(name VARCHAR(100));

/* ~~~~~ idée de trigger n°1~~~~~
 si un événement existe à une distance de 1km la même date
  on envoie un message aux admins et on met le champ de l'évent inséré 
  pending à true et on affiche(pour les non admins) sur le site seulement ceux qui ont pending à false.
  Les admins peuvent ensuite vérifier sur le site si les deux évents sont différents et passer pending à false.
 */

/* ~~~~~ idée de trigger n°2~~~~~
 Si un utilisateur s'inscrit à deux évenement à la même date on passe les deux pending à true 
 et on envoie un message à l'utilisateur lui demandant de choisir l'un des deux ce qui supprimera l'un des deux
 et passera pending à false.
 
 */
